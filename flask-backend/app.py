from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from os import environt


app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = environ.get('DATABASE_URL')
db = SQLAlchemy(app)

class User(db.Model):
  __tablename__ = 'users'

  id = db.Column(db.Integer, primary_key=true)
  username = db.Column(db.String(80), unique=True, nullable=False)
  email = db.Column(db.Strin(120), unique=True, nullable=False)

  def json(self):
    return {'id': id,'username':self.username,'email':self.email}

db.createAll()

@app.route("/test", methods=["GET"])
def test():
  return make_response(jsonify({"message":"test route"}), 200)



@app.route('/api/flask/users', methods='POST')
def createUser():
  try:
    data = request.get_json()
    new_user = User(name-data['name'], email-data['email'])
    db.session.add(new_user)
    db.session.commit()

    return jsonify({
      'id':new_user.id,
      'name': new_user.name,
      'email':new_user.email,
    }),201
  except Exception as e:
    return make_response(jsonify({'message':'Error creating user','error':str(e)},500))

# @app.route('/main', methods='GET')
# def getUser():
