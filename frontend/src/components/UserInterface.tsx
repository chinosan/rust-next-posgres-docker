'use client'
import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react'
import Card from './Card';
import useFetch from '@/hooks/useFetch';

export interface User {
  id: number;
  name: string;
  email: string;
}

type UIProps = {
  backendName: string
}
const inputStyle = 'border-gray-300 rounded mb-2 w-full  p-2 border text-black';
const backgroundColors: { [key: string]: string } = {
  rust: "bg-orange-500"
};
const buttonColors: { [key: string]: string } = { rust: "bg-orange-700 hover:bgorange-600" };
const initialNewUser = { name: "", email: "" }
const initialUpdateUser = { ...initialNewUser, id: "" }

const UserInterface = ({ backendName }: UIProps) => {
  const bgColor = backgroundColors[backendName as keyof typeof backgroundColors] || "bg-gray-500";
  const btnColor = buttonColors[backendName as keyof typeof buttonColors];
  const api_url = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:8080';
  const {
    fetchData,
    createUser,
    deleteUser,
    updateUser
  } = useFetch({ api_url, backendName });
  const [users, setUsers] = useState<User[]>([])
  const [newUser, setNewUser] = useState(initialNewUser)
  const [updateUserData, setUpdateUserData] = useState(initialUpdateUser)

  useEffect(() => {
    const fetchUsers = async () => {
      const res = await fetchData();
      setUsers(res)
      console.log(res)
    }
    fetchUsers();
  }, [backendName, api_url])

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      console.log(newUser)
      const res = await createUser(newUser)
      console.log("added", res)
      setUsers([...users, res])
      setNewUser(initialNewUser)
    } catch (error) {
      console.error("addUser", error)
    }
  }

  const handleDeleteUser = async (id: number) => {
    try {
      await deleteUser(id)
      setUsers(users.filter(usr => usr.id != id))
    } catch (error) {
      console.error('Delete user ', error)
    }
  }
  const handleUpdateUser = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      await updateUser(updateUserData);
      setUsers(
        users.map(usr => {
          if (usr.id == Number(updateUserData.id))
            return { ...updateUserData, id: Number(updateUserData.id) }
          return usr
        })
      )
      setUpdateUserData(initialUpdateUser)
    } catch (error) {
      console.error("update user", error)
    }
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.target;
    setNewUser({ ...newUser, [name]: value })
  }

  return (
    <div
      className={`user-interface ${bgColor} ${backendName} w-full max-w-md p-4 my-4 rounded shadow`}
    >
      <img src={`/${backendName}logo.svg`}
        alt={`${backendName} Logo`} className="w-20 h-20 mb-6 mx-auto"
      />
      <h2
        className="text-xl font-bold text-center text-white mb-6"
      >
        {`${backendName.charAt(0).toUpperCase() + backendName.slice(1)} Backend`}
      </h2>

      {/* Form to add new user */}
      <form
        onSubmit={handleSubmit}
        className='bg-blue-100 p-4 rounded shadow mb-6' >
        <input type='text'
          placeholder='name' name='name'
          className={inputStyle}
          value={newUser.name} onChange={handleChange}
        />
        <input
          placeholder='Email' name='email'
          className={inputStyle}
          value={newUser.email} onChange={handleChange}
        />
        <button
          type='submit'
          disabled={newUser.email == "" && newUser.name == ""}
          className='bg-blue-500 rounded w-full p-2 hover:bg-blue-600 text-white'
        >
          Add
        </button>
      </form>
      {/* Form to update user */}
      <form onSubmit={handleUpdateUser} className="mb-6 p-4 bg-blue-100 rounded shadow">
        <input
          placeholder="User ID"
          value={updateUserData.id}
          onChange={(e) => setUpdateUserData({ ...updateUserData, id: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />
        <input
          placeholder="New Name"
          value={updateUserData.name}
          onChange={(e) => setUpdateUserData({ ...updateUserData, name: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />
        <input
          placeholder="New Email"
          value={updateUserData.email}
          onChange={(e) => setUpdateUserData({ ...updateUserData, email: e.target.value })}
          className="mb-2 w-full p-2 border border-gray-300 rounded"
        />
        <button type="submit" className="w-full p-2 text-white bg-green-500 rounded hover:bg-green-600">
          Update User
        </button>
      </form>

      <div className='space-y-4' >
        {
          users.map(user => (
            <div className='flex items-center justify-between rounded-lg shadow bg-white p-4' key={"user-item" + user.id}>
              <Card
                {...user}
              />
              <button
                onClick={() => handleDeleteUser(user.id)}
                className={`${btnColor} text-white py-2 px-4 rounded`}
              >Delete</button>
            </div>
          ))
        }
      </div>
      {/* <Card /> */}
    </div>
  )
}

export default UserInterface