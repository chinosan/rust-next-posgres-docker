import React from 'react'
import { User } from './UserInterface'

export type Cardsprops = {
  id: string,
  name: string,
  email: string
}


const Card = ({ email, name, id }: User) => {
  return (
    <div
      className='bg-white rounded-lg p-2 mb-2 shadow-lg hover:bg-gray-100'
    >
      <p className='text-sm text-gray-600'>ID:{id}</p>
      <p className='text-lg text-gray-800 font-semibold'>nombre: {name}</p>
      <p className='text-md text-gray-700'>email:{email}</p>
    </div>
  )
}

export default Card