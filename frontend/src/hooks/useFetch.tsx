type FetchProps = {
  api_url: string,
  backendName: string
}
type NewUser = {
  email: string,
  name: string
}
type UpdateUser = NewUser & {
  id: string
}

const useFetch = ({ api_url, backendName }: FetchProps) => {
  const base_url = `${api_url}/api/${backendName}/users`

  const fetchData = async () => {
    try {
      const res = await fetch(base_url, { method: "GET", headers: getHeaders() });
      // debugger;
      const data = res.json();
      // debugger;
      return data;
    } catch (error) {
      console.error(error)
      return [];
    }
  }

  const createUser = async (newUser: NewUser) => {
    try {
      const res = await fetch(base_url,
        {
          method: "POST",
          headers: getHeaders(),
          body: JSON.stringify(newUser)
        })
      const data = res.json();
      return data;
    } catch (error) {
      return Promise.reject(error)
    }
  }
  const updateUser = async (updateUser: UpdateUser) => {
    try {
      const res = await fetch(`${base_url}/${updateUser.id}`,
        {
          method: "PUT",
          headers: getHeaders(),
          body: JSON.stringify(updateUser)
        })
    } catch (error) {
      console.error("Error updating user", error)
    }
  }
  const deleteUser = async (id: number) => {
    try {
      await fetch(`${base_url}/${id}`, {
        method: "DELETE",
        headers: getHeaders()
      })
    } catch (error) {
      return Promise.reject(error)
    }
  }

  return {
    fetchData,
    createUser,
    updateUser,
    deleteUser
  }
}

export default useFetch;

function getHeaders() {
  return {
    // "Accept": "application/json",
    'Content-Type': 'application/json',
    // 'Access-Control-Allow-Origin': '*',
    // 'Access-Control-Allow-Headers': 'Content-Type',
    // 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'
  }
}